# README #

## Introduction:  

This is a simple To-Do application to practice my skills in React JS. This application uses Firebase API to persist data.


## Technologies: ##

* HTML
* CSS
* ReactJS
* Firebase

## Purpose: ##

* Learn and apply ReactJS framework to create an application.
* Integrate the Firebase API to make the application run in realtime.

## Procedure: ##

1. Clone the repository.
2. Use command prompt to run the Gulp file.



## Timestamp: ##

**May 2015 – June 2015